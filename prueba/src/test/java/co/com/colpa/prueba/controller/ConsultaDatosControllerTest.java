package co.com.colpa.prueba.controller;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import co.com.colpa.prueba.dto.ResponseEstandarWSRestDTO;
import co.com.colpa.prueba.payload.GeneralReq;
import co.com.colpa.prueba.payload.DetallePeticion;
import co.com.colpa.prueba.service.ConsultaDatosService;

@SpringBootTest
class ConsultaDatosControllerTest {

	@InjectMocks
	private ConsultaDatosController consultaDatosController;
	
	@Mock
	private ConsultaDatosService consultaDatosService;
	
	@BeforeEach
    void setup() {
	 	MockitoAnnotations.openMocks(consultaDatosController);
    }
	
	@Test
	void testGetDatos() {
		GeneralReq<DetallePeticion> request = new GeneralReq<>();
		DetallePeticion consulta = new DetallePeticion();
		request.setRequestBody(consulta);
		ResponseEstandarWSRestDTO responseEstandarWSRestDTO = new ResponseEstandarWSRestDTO();
		when(consultaDatosService.getDatos(consulta)).thenReturn(responseEstandarWSRestDTO);
		ResponseEstandarWSRestDTO result = consultaDatosController.getDatos(request);
		System.out.println(result);
	    System.out.println(responseEstandarWSRestDTO);
	    assertEquals(responseEstandarWSRestDTO,result);
	}
	
	

}

package co.com.colpa.prueba.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.colpa.prueba.dto.ResponseEstandarWSRestDTO;
import co.com.colpa.prueba.payload.GeneralReq;
import co.com.colpa.prueba.payload.DetallePeticion;
import co.com.colpa.prueba.service.ConsultaDatosService;

@RestController
@RequestMapping("/prueba/v1/consulta")
public class ConsultaDatosController {
	
	private final String ENDPOINT_CONSULTA = "/consultaDatos";
	
	@Autowired
	ConsultaDatosService consultaDatosService;
	
	@ResponseBody
	@RequestMapping(value = ENDPOINT_CONSULTA, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEstandarWSRestDTO getDatos(@RequestBody GeneralReq<DetallePeticion> request) {
		return consultaDatosService.getDatos(request.getRequestBody());
	}

}

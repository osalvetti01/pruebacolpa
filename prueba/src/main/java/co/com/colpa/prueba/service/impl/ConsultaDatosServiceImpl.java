package co.com.colpa.prueba.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import co.com.colpa.prueba.dto.ResponseEstandarWSRestDTO;
import co.com.colpa.prueba.enums.CodigoRespuestaEnum;
import co.com.colpa.prueba.payload.DetallePeticion;
import co.com.colpa.prueba.service.ConsultaDatosService;

@Service
public class ConsultaDatosServiceImpl implements ConsultaDatosService{

	
	private String location_info_endpoint;
	
	@Autowired
	RestTemplate restTemplate;
	
	@Override
	public ResponseEstandarWSRestDTO getDatos(DetallePeticion request) {
		ResponseEstandarWSRestDTO respuestaeDTO = new ResponseEstandarWSRestDTO();
		try {
			
			String endpoint = "https://my.api.mockaroo.com/clientes/123.json?key=61f765d0";
			//String endpoint = "https://my.api.mockaroo.com/users.json?key=61f765d0";
			RestTemplate restTemplate = this.restTemplate;
			ResponseEntity<String> response = restTemplate.exchange(endpoint, HttpMethod.GET,null,String.class);
			if(response.getStatusCode() == HttpStatus.OK) {
				respuestaeDTO.setCodRespuesta(CodigoRespuestaEnum.OK.getIdCodigoRespuesta());
				respuestaeDTO.setRespuestaServicio(response.getBody());
				String respuesta = response.getBody();
//				System.out.println(respuesta);
//				JSONObject json = new JSONObject(respuesta); 
//				JSONObject error = json.getJSONObject("error");
//				JSONObject code = new JSONObject(error.toString());
//				String descripcion = code.getString("description");
//				int codigo = code.getInt("code");
//				if (codigo >= 10) {
//					clienteQualitiesResponseDTO.setMensajeError(descripcion);
//				} else {
//					clienteQualitiesResponseDTO.setMensajeError("");
//				}
//				
			}else {
				respuestaeDTO.setCodRespuesta(CodigoRespuestaEnum.ERROR_CONTROLADO.getIdCodigoRespuesta());
				respuestaeDTO.setRespuestaServicio(response.getBody());
			}
			
		} catch (Exception ex) {
			respuestaeDTO.setCodRespuesta(CodigoRespuestaEnum.ERROR_TECNICO.getIdCodigoRespuesta());
			respuestaeDTO.setMensajeError(ex.getMessage());
		}
		return respuestaeDTO;
	}

}

package co.com.colpa.prueba.service;

import co.com.colpa.prueba.dto.ResponseEstandarWSRestDTO;
import co.com.colpa.prueba.payload.DetallePeticion;

public interface ConsultaDatosService {
	ResponseEstandarWSRestDTO getDatos(DetallePeticion request);
}

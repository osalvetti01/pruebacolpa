package co.com.colpa.prueba.enums;


public enum CodigoRespuestaEnum {
    
    OK(1),
    ERROR_CONTROLADO(2),
    ERROR_TECNICO(3);
    
    private Integer idCodigoRespuesta;
    
    CodigoRespuestaEnum(Integer idCodigoRespuesta){
        this.idCodigoRespuesta=idCodigoRespuesta;
    }
    

    public Integer getIdCodigoRespuesta() {
        return idCodigoRespuesta;
    }

    
}
